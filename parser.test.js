const Big = require('big.js')

const { parse, head, body } = require('./')

describe('PARSE HEAD', () => {
  test('empty lines', () => {
    expect(head('')).toBe(false)
    expect(head('  ')).toBe(false)
    expect(head('  \n')).toBe(false)
  })
  test('date', () => {
    expect(head('2021/1/1').date)
      .toEqual(new Date('2021/1/1'))
    expect(head('2021/1/11 1').date)
      .toEqual(new Date('2021/1/11'))
    expect(head('2021/1/11 2 Ab').date)
      .toEqual(new Date('2021/1/11'))
  })
  test('title', () => {
    expect(head('2021/1/1 Foo bar'))
    .toEqual({
      date: new Date('2021/1/1'),
      title: 'Foo bar',
      cleared: false
    })
  })
  test('cleared', () => {
    expect(head('2021/1/1 * Foo bar'))
      .toEqual({
        date: new Date('2021/1/1'),
        title: 'Foo bar',
        cleared: true
      })
  })
})

describe('PARSE BODY LINE', () => {
  test('wrong', () => {
    expect(body('2021/1/1 Foo bar')).toBe(false)
  })
  test('leading spaces', () => {
    expect(body('Foo  42 EUR')).toBe(false)
    expect(body(' Foo  42 EUR')).toBe(false)
  })
  test('account', () => {
    expect(body('  Foo').account).toEqual('Foo')
    expect(body('    Foo').account).toEqual('Foo')
    expect(body('  Foo:Bar  ').account).toEqual('Foo:Bar')
    expect(body('  Foo  € 42     ').account).toEqual('Foo')
    expect(body('  Foo:Bar  42EUR').account).toEqual('Foo:Bar')
    expect(body('  F:B a:Ba  42EUR').account).toEqual('F:B a:Ba')
  })
  test('amount', () => {
    expect(body('  Foo  42 EUR').amount).toEqual(Big(42))
    expect(body('  Foo  -42 EUR').amount).toEqual(Big(-42))
    expect(body('  Foo  4.2 EUR').amount).toEqual(Big(4.2))
  })
  test('currency', () => {
    expect(body('  Foo  42 EUR').curr).toBe('EUR')
    expect(body('  Foo  42EUR').curr).toBe('EUR')
    expect(body('  Foo  € 42').curr).toBe('€')
    expect(body('  Foo  €42').curr).toBe('€')
  })
  test('all together', () => {
    expect(body('  Foo:Ba r:Baz  42EUR'))
      .toEqual({
        account: 'Foo:Ba r:Baz',
        amount: Big(42),
        curr: 'EUR'
    })
  })
})


const file = `
2020/1/1 * title
  Foo:Bar  42EUR
  Baz:Bla

2020/1/2 title 333
  Foo:B ar  €-42.3
  Baz:Bla
`
describe('PARSE FILE', () => {
  test('parse two transactions', () => {
    expect(parse(file)).toEqual([
      {
        date: new Date('2020/1/1'),
        title: 'title',
        cleared: true,
        body: [
          { account: 'Foo:Bar',
            amount: Big(42),
            curr: 'EUR'
          },
          { account: 'Baz:Bla',
            amount: Big(-42),
            curr: 'EUR'
          }
        ]
      },
      {
        date: new Date('2020/1/2'),
        title: 'title 333',
        cleared: false,
        body: [
          { account: 'Foo:B ar',
            amount: Big(-42.3),
            curr: '€'
          },
          { account: 'Baz:Bla',
            amount: Big(42.3),
            curr: '€'
          }
        ]
      }
    ])
  })
})
