const Big = require('big.js')
Big.RM = 3 // rounding

// parse headline of ledger transaction
const head = (txt) => {
  txt = txt.split(';')[0] // remove comments
  const m = txt.match(/(\d\d\d\d\/\d+\/\d+)(.*)/)
  if (m) {
    const date = new Date(m[1])
    const title = m[2].split(' * ')
    if (title.length > 1)
      return { date, cleared: true, title: title[1].trim() }
    else return { date, cleared: false, title: title[0].trim() }
  }
  return false
}

const body = (txt) => {
  txt = txt.split(';')[0] // remove comments
  const m = txt.match(/\s\s+(.*?)\s\s(.*)/)
  if (m) {
    const account = (m[1] || m[2]).trim()
    const mm = m[2].match(/([^\d+-]+\s?)?([+-]?([0-9]*[.])?[0-9]+)\s?(.*)/)
    if (mm)
      return { account,
        amount: Big(mm[2]),
        curr: (mm[1] || mm[4]).trim() }
    else return { account }
  } else if (txt.match(/^\s\s+(.*)/)) {
    return { account: txt.trim() }
  }
  return false
}

const parse = (file) => {
  const txns = []
  let tx, sum = Big(0)
  for (line of file.split('\n')) {
    if (head(line)) {
      if (!sum.eq(Big(0))) throw new Error('does not balance')
      tx = {...head(line), body: []}
      txns.push(tx)
      sum = Big(0)
    } else if (body(line)) {
      tx.body.push(body(line))
      if (body(line).amount) {
        sum = sum.add(body(line).amount)
      } else {
        tx.body[tx.body.length-1].amount = sum.times(-1)
        tx.body[tx.body.length-1].curr = tx.body[0].curr
        sum = Big(0)
      }
    }
  }
  if (sum == 0) {
    return txns
  } else return new Error('txn does not balance')
}

module.exports = { parse, head, body }
