# ledger-parser

parse [plaintextaccounting.org](https://plaintextaccounting.org)
*ledger* format
into a *js object*

-----------

### Documentation

see [tests](parser.test.js)


### Run Tests

    git clone git@gitlab.com:orangeman/ledger-parser.git
    cd ledger-parser
    npm install
    npm test


-------------

### License

MIT
